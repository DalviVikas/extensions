
Pod::Spec.new do |s|

  s.name         = "extensions"
  s.version      = "0.2.2"
  s.summary      = "Extending the functionality of existing classes and structures in swift."


  s.homepage     = "https://bitbucket.org/DalviVikas/extensions"

  s.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  s.author       = { "Vikas Dalvi" => "vikasdalvi.29@gmail.com" }

  s.platform     = :ios
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://DalviVikas@bitbucket.org/DalviVikas/extensions.git", :tag => "0.2.2" }

  s.source_files  = "extensions", "extensions/**/*.{swift}"

  s.module_name = "Extensions"

  s.framework  = "UIKit"

  s.requires_arc = true

end
