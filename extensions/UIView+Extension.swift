//
//  UIView+Extension.swift
//  extension
//
//  Created by Robo-Vikas on 21/02/18.
//  Copyright © 2018 robomate. All rights reserved.
//

import UIKit

// MARK: Shadow extension
public extension UIView {
    
    func addShadow(_ color: UIColor = .black,
                          offset: CGSize = CGSize(width: 3.0, height: 3.0),
                          radius: CGFloat = 3.0,
                          opacity: Float = 0.3) {
        
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.masksToBounds = false
    }
}



// MARK: Border extension
public extension UIView {
    
    func addBorder(_ color: UIColor = .black, width: CGFloat = 1.0) {
        
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
}


// MARK: Rounded Corner
public extension UIView {
    
    var cornerRadius: CGFloat {
        
        set { self.layer.cornerRadius = newValue }
        
        get { return self.layer.cornerRadius }
    }
}


// MARK: SubView
public extension UIView {
    
    func add(_ view: UIView) {
        
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let dictionary = ["view" : view]
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: dictionary)
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|",
                                                                 options: [],
                                                                 metrics: nil,
                                                                 views: dictionary)
        
        self.addConstraints(horizontalConstraints)
        self.addConstraints(verticalConstraints)
        self.bringSubviewToFront(view)
    }
    
    func insert(_ view: UIView, below sibling: UIView) {
        
        self.insertSubview(view, belowSubview: sibling)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let dictionary = ["view" : view]
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: dictionary)
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|",
                                                                 options: [],
                                                                 metrics: nil,
                                                                 views: dictionary)
        
        self.addConstraints(horizontalConstraints)
        self.addConstraints(verticalConstraints)
    }
    
    func insert(_ view: UIView, above sibling: UIView) {
        
        self.insertSubview(view, aboveSubview: sibling)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let dictionary = ["view" : view]
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: dictionary)
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|",
                                                                 options: [],
                                                                 metrics: nil,
                                                                 views: dictionary)
        
        self.addConstraints(horizontalConstraints)
        self.addConstraints(verticalConstraints)
    }
}
