//
//  UIAlertController+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 14/03/18.
//  Copyright © 2018 scio. All rights reserved.
//

import Foundation

public extension UIAlertController {
    
    typealias Action = (title: String, style: UIAlertAction.Style)
    
    class func alert(_ title: String? = "",
                            message: String? = "",
                            preferredStyle: UIAlertController.Style = .alert, actions: [Action]?,
                            handler: ((_ alertAction: UIAlertAction) -> Void)?) -> UIAlertController {
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: preferredStyle)
        
        if let actions = actions {
            
            for action in actions {
                
                let alertAction = UIAlertAction.init(title: action.title, style: action.style, handler: { (alertAction) in
                    
                    if let handler = handler { handler(alertAction) }
                    
                })
                
                alertController.addAction(alertAction)
            }
        }
        
        return alertController
    }
}
