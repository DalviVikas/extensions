//
//  UIDevice+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 21/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import UIKit

public extension UIDevice {
    
    var iPad: Bool { return self.userInterfaceIdiom == .pad }
    var iPhone: Bool { return self.userInterfaceIdiom == .phone }
    
    @available(iOS 9.0, *)
    var tv: Bool { return self.userInterfaceIdiom == .tv }
    
    @available(iOS 9.0, *)
    var carPlay: Bool { return self.userInterfaceIdiom == .carPlay }    
}
