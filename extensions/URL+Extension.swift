//
//  URL+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 08/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import Foundation

public extension URL {
 
    var queryParameters: [String:String]? {
        
        guard let urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: false), let queryItems = urlComponents.queryItems else {
            return nil
        }
        
        var items: [String:String] = [:]
        
        for item in queryItems {
            items[item.name] = item.value
        }
        
        return items
    }
    
    func appendingQueryParameters(_ parameters: [String:Any]) -> URL {
        
        var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        
        var items = urlComponents.queryItems ?? []
        items += parameters.map { URLQueryItem(name: $0.key, value: "\($0.value)")}
        urlComponents.queryItems = items
        
        return urlComponents.url!
    }
    
    mutating func appendQueryParameters(_ parameters: [String:Any]) {
        self = appendingQueryParameters(parameters)
    }
}
