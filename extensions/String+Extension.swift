//
//  String+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 12/03/18.
//  Copyright © 2018 scio. All rights reserved.
//

import Foundation


// MARK: - String Validation Extension
public extension String {
    
    var isAlphaNumeric: Bool {
        let characterSet = CharacterSet.alphanumerics.inverted
        return (self.rangeOfCharacter(from: characterSet) == nil)
    }
    
    var isNumeric: Bool {
        let characterSet = CharacterSet.init(charactersIn: "0123456789").inverted
        return (self.rangeOfCharacter(from: characterSet) == nil)
    }
    
    var isValidEmailId: Bool {
     
        let maxLegnth = 254
        
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let prdicate = NSPredicate(format: "SELF MATCHES %@", regex)
        let evaluate = prdicate.evaluate(with: self)
        
        return evaluate && self.count <= maxLegnth
    }
}


// MARK: - Base 64 encoder and decoder
public extension String {
    
    var encodeBase64: String? {
        let data = self.data(using: .utf8)
        return data?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
    
    var decodeBase64: String? {
        
        if let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) {
        
            if let string = String.init(data: data, encoding: .utf8) {
                return string
            }
        }
        
        return self
    }
}
