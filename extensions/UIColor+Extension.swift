//
//  UIColor+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 08/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import UIKit

public extension UIColor {
    
    convenience init?(hexCode: String) {
        
        guard hexCode.count > 6 else { return nil }
        
        if hexCode.hasPrefix("#") {
            
            let start = hexCode.index(hexCode.startIndex, offsetBy: 1)
            let hexColor = String(hexCode[start...])
            
            if hexColor.count == 8 {
                
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    
                    let red = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    let green = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    let blue = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    let alpha = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: red, green: green, blue: blue, alpha: alpha)
                    return
                }
                
            } else if hexColor.count == 6 {
                
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    
                    let red = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    let green = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    let blue = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: red, green: green, blue: blue, alpha: 1.0)
                    return
                }
            }
        }
        
        return nil
    }
    
    class var random: UIColor {
        
        let red = CGFloat(arc4random_uniform(255)) / 255.0
        let green = CGFloat(arc4random_uniform(255)) / 255.0
        let blue = CGFloat(arc4random_uniform(255)) / 255.0
        
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
}
