//
//  Date+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 08/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import Foundation

// MARK: - Date Formatters
public extension Date {

    enum Format: String {
        case `default` = "yyyy-MM-dd HH:mm:ss"
        case longDateTime = "yyyy-MM-dd HH:mm:ss.SSS"
        case indian = "dd/MM/yyyy"
        case yyyy_MM_dd = "yyyy-MM-dd"
    }
    
    enum DayName: String {
        
        case initial = "EEEEE"
        case short = "EEE"
        case full = "EEEE"
    }
    
    enum MonthName: String {
        
        case initial = "MMMMM"
        case short = "MMM"
        case full = "MMMM"
    }
    
    static func dateFromString(_ string: String, format: String = Format.default.rawValue) -> Date? {
        
        let dateString = string.replacingOccurrences(of: "T", with: " ")
        
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: dateString)
    }
    
    /// Get string from date in specified format
    ///
    /// - Parameter format: date format
    /// - Returns: Formatted date string
    func string(withFormat format: String = Format.default.rawValue) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    /// creates formatted date string
    ///
    /// - Parameter style: format to apply
    ///
    ///  .short = 1/12/17
    ///  .medium = Jan 12, 2017
    ///  .long = January 12, 2017
    ///  .full = Thursday, January 12, 2017
    ///
    /// - Returns: formatted date string
    func dateString(ofStyle style: DateFormatter.Style = .medium) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = style
        dateFormatter.timeStyle = .none
        
        return dateFormatter.string(from: self)
    }
    
    
    /// creates formatted dateTime string
    ///
    /// - Parameter style: format to apply
    ///
    ///  .short = 1/12/17, 7:32 PM
    ///  .medium = Jan 12, 2017, 7:32:00 PM
    ///  .long = January 12, 2017 at 7:32:00 PM GMT+3
    ///  .full = Thursday, January 12, 2017 at 7:32:00 PM GMT+03:00
    ///
    /// - Returns: formatted dateTime string
    func dateTimeString(ofStyle style: DateFormatter.Style = .medium) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = style
        dateFormatter.timeStyle = style
        
        return dateFormatter.string(from: self)
    }
    
    
    /// Creates formatted time string
    ///
    /// - Parameter style: format to apply
    ///
    ///  .short = 7:37 PM
    ///  .medium = 7:37:02 PM
    ///  .long = 7:37:02 PM GMT+3
    ///  .full = 7:37:02 PM GMT+03:00
    ///
    /// - Returns: formatted time string
    func timeString(ofStyle style: DateFormatter.Style = .medium) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = style
        
        return dateFormatter.string(from: self)
    }
    
    
    /// Day string in specified format
    ///
    /// - Parameter style: format
    /// - Returns: formated day string
    func dayName(ofStyle style: DayName = DayName.short) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate(style.rawValue)
        return dateFormatter.string(from: self)
    }
    
    /// Month string in specified format
    ///
    /// - Parameter style: format
    /// - Returns: formated month string
    func monthName(ofStyle style: MonthName = MonthName.short) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate(style.rawValue)
        return dateFormatter.string(from: self)
    }
}



// MARK: - Get & Set Date components
public extension Date {
    
    func adding(_ component: Calendar.Component, value: Int) -> Date {
        return Calendar.current.date(byAdding: component, value: value, to: self)!
    }
    
    mutating func add(_ component: Calendar.Component, value: Int) {
        let date = self.adding(component, value: value)
        self = date
    }
    
    var year: Int {
        
        get { return Calendar.current.component(.year, from: self) }
        
        set {
            
            guard newValue > 0 else { return }
            
            let yearsToAdd = newValue - Calendar.current.component(.year, from: self)
            
            if let date = Calendar.current.date(byAdding: .year, value: yearsToAdd, to: self) { self = date }
        }
    }
    
    var month: Int {
        
        get { return Calendar.current.component(.month, from: self) }
        
        set {
            
            let allowedRanges = Calendar.current.range(of: .month, in: .year, for: self)!
            
            guard allowedRanges.contains(newValue) else { return }
        
            let monthsToAdd = newValue - Calendar.current.component(.month, from: self)
            if let date = Calendar.current.date(byAdding: .month, value: monthsToAdd, to: self) { self = date }
        }
    }
    
    var day: Int {
        
        get { return Calendar.current.component(.day, from: self) }
        
        set {
            
            let allowedRanges = Calendar.current.range(of: .day, in: .month, for: self)!
            
            guard allowedRanges.contains(newValue) else { return }
            
            let daysToAdd = newValue - Calendar.current.component(.day, from: self)
            if let date = Calendar.current.date(byAdding: .day, value: daysToAdd, to: self) { self = date }
        }
    }
    
    var hour: Int {
        
        get { return Calendar.current.component(.hour, from: self) }
        
        set {
            
            let allowedRanges = Calendar.current.range(of: .hour, in: .day, for: self)!
            
            guard allowedRanges.contains(newValue) else { return }
            
            let hoursToAdd = newValue - Calendar.current.component(.hour, from: self)
            if let date = Calendar.current.date(byAdding: .hour, value: hoursToAdd, to: self) { self = date }
        }
    }
    
    var minute: Int {
        
        get { return Calendar.current.component(.minute, from: self) }
        
        set {
            
            let allowedRanges = Calendar.current.range(of: .minute, in: .hour, for: self)!
            
            guard allowedRanges.contains(newValue) else { return }
            
            let minutesToAdd = newValue - Calendar.current.component(.minute, from: self)
            if let date = Calendar.current.date(byAdding: .minute, value: minutesToAdd, to: self) { self = date }
        }
    }
    
    var second: Int {
        
        get { return Calendar.current.component(.second, from: self) }
        
        set {
            
            let allowedRanges = Calendar.current.range(of: .second, in: .minute, for: self)!
            
            guard allowedRanges.contains(newValue) else { return }
            
            let secondsToAdd = newValue - Calendar.current.component(.second, from: self)
            if let date = Calendar.current.date(byAdding: .second, value: secondsToAdd, to: self) { self = date }
        }
    }
    
    var nanoseond: Int {
        
        get { return Calendar.current.component(.nanosecond, from: self) }
        
        set {
            
            let allowedRanges = Calendar.current.range(of: .nanosecond, in: .second, for: self)!
            
            guard allowedRanges.contains(newValue) else { return }
            
            let nanosecondsToAdd = newValue - Calendar.current.component(.nanosecond, from: self)
            if let date = Calendar.current.date(byAdding: .nanosecond, value: nanosecondsToAdd, to: self) { self = date }
        }
    }
    
    
    var millisecond: Int {
        
        get { return self.nanoseond / 1000000 }
        
        set {
            
            let ns = newValue * 1000000
            self.nanoseond = ns
        }
    }
    
    
    var dayOfWeek: Int { return Calendar.current.component(.weekday, from: self) }
    
    var weekOfMonth: Int { return Calendar.current.component(.weekOfMonth, from: self) }
    
    var weekOfYear: Int { return Calendar.current.component(.weekOfYear, from: self) }
    
}

// MARK: - Compare
public extension Date {
    
    var isInFuture: Bool { return self > Date() }
    
    var isInPast: Bool { return self < Date() }
    
    var isInToday: Bool { return Calendar.current.isDateInToday(self) }
    
    var isInTomorrow: Bool { return Calendar.current.isDateInTomorrow(self) }
    
    var isInYesterday: Bool { return Calendar.current.isDateInYesterday(self) }
    
    var isInWeekend: Bool { return Calendar.current.isDateInWeekend(self) }
    
    var isWorkday: Bool { return !self.isInWeekend }
    
    var isInCurrentWeek: Bool { return Calendar.current.isDate(self, equalTo: Date(), toGranularity: .weekOfYear) }
    
    var isInCurrentMonth: Bool { return Calendar.current.isDate(self, equalTo: Date(), toGranularity: .month) }
    
    var isInCurrentYear: Bool { return Calendar.current.isDate(self, equalTo: Date(), toGranularity: .year) }
    
}


// MARK: - Static Methods
extension Date {
    
    public static func days(`in` month: Int, `in` year: Int) -> Int {
        
        var components = DateComponents()
        components.month = month
        components.year = year
        
        let date = Calendar.current.date(from: components)!
        return Calendar.current.range(of: .day, in: .month, for: date)?.count ?? 0
    }
    
    public static func days(from date1: Date, to date2: Date) -> Int {
    
        let components = Calendar.current.dateComponents([.day], from: date1, to: date2)
        return components.day ?? 0
    }
}


