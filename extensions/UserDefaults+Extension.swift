//
//  UserDefaults+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 08/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import Foundation

public extension UserDefaults {
    
    subscript(key: String) -> Any? {
        
        get { return object(forKey: key) }
        
        set { set(newValue, forKey: key) }
    }
}
