//
//  UIFont+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 08/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import UIKit

public extension UIFont {
    
    private enum Font: String {
        
        case robo = "robo"
        case helveticaNeue = "HelveticaNeue"
        case helveticaNeueBold = "HelveticaNeue-Bold"
        case helveticaNeueMedium = "HelveticaNeue-Medium"
        case helveticaNeueLight = "HelveticaNeue-Light"
        case helveticaNeueItalic = "HelveticaNeue-Italic"
        case helveticaNeueLightItalic = "HelveticaNeue-LightItalic"
        case helveticaNeueUltraLight = "HelveticaNeue-UltraLight"
    }
    
    class func robo(of size: CGFloat) -> UIFont? {
        return UIFont(name: Font.robo.rawValue, size: size)
    }
    
    class func helveticaNeue(of size: CGFloat) -> UIFont? {
        return UIFont(name: Font.helveticaNeue.rawValue, size: size)
    }
    
    class func helveticaNeueBold(of size: CGFloat) -> UIFont? {
        return UIFont(name: Font.helveticaNeueBold.rawValue, size: size)
    }
    
    class func helveticaNeueMedium(of size: CGFloat) -> UIFont? {
        return UIFont(name: Font.helveticaNeueMedium.rawValue, size: size)
    }
    
    class func helveticaNeueLight(of size: CGFloat) -> UIFont? {
        return UIFont(name: Font.helveticaNeueLight.rawValue, size: size)
    }
    
    class func helveticaNeueItalic(of size: CGFloat) -> UIFont? {
        return UIFont(name: Font.helveticaNeueItalic.rawValue, size: size)
    }
    
    class func helveticaNeueLightItalic(of size: CGFloat) -> UIFont? {
        return UIFont(name: Font.helveticaNeueLightItalic.rawValue, size: size)
    }
    
    class func helveticaNeueUltraLight(of size: CGFloat) -> UIFont? {
        return UIFont(name: Font.helveticaNeueUltraLight.rawValue, size: size)
    }
}
