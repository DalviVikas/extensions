//
//  UIViewController+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 21/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import UIKit

public extension UIViewController {
    
    func add(_ controller: UIViewController) {
        
        controller.willMove(toParent: self)
        self.addChild(controller)
        self.view.add(controller.view)
        controller.didMove(toParent: self)
    }
    
    func remove() {
        
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    func insert(_ controller: UIViewController, below sibling: UIViewController) {
        
        controller.willMove(toParent: self)
        controller.addChild(controller)
        self.view.insert(controller.view, below: sibling.view)
        controller.didMove(toParent: self)
    }
    
    func insert(_ controller: UIViewController, above sibling: UIViewController) {
        
        controller.willMove(toParent: self)
        controller.addChild(controller)
        self.view.insert(controller.view, above: sibling.view)
        controller.didMove(toParent: self)
    }
    
    
}
