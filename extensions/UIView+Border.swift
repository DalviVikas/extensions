//
//  UIView+Border.swift
//  extensions
//
//  Created by Robo-Vikas on 28/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import UIKit

// MARK: Border extension

public typealias Insets = (left: CGFloat, right: CGFloat)


public protocol BorderProtocol {
    
    var color: UIColor { get set }
    var thickness: CGFloat { get set }
    var insets: Insets { get set }
}

public struct Side: OptionSet {
    
    public let rawValue: Int
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
    
    public static let left = Side(rawValue: 1 << 0)
    public static let top = Side(rawValue: 1 << 1)
    public static let right = Side(rawValue: 1 << 2)
    public static let bottom = Side(rawValue: 1 << 3)
    
    public static let all: Side = [.left, .top, .right, .bottom]
}

public extension UIView {
    
    class Border: UIView, BorderProtocol {
        
        public var color: UIColor {
            
            didSet {
                self.backgroundColor = color
            }
        }
        
        public var thickness: CGFloat
        public var insets: Insets
        
        fileprivate init(color: UIColor = .black, thikness: CGFloat = 1.0, insets: Insets = (0.0, 0.0), roundedCorner: Bool) {
            
            self.color = color
            self.thickness = thikness
            self.insets = insets
            
            super.init(frame: .zero)
            
            self.backgroundColor = color
            
            if roundedCorner {
                self.layer.cornerRadius = thikness * 0.5
            }
        }
        
        required public init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    func addBorder(_ side: Side = .all, color: UIColor = .black, thickness: CGFloat = 1.0, insets: Insets = (0.0, 0.0), roundedCorner: Bool = false) {
        
        if side.contains(.left) {
            self.addLeftBorder(color: color, thickness: thickness, insets: insets, roundedCorner: roundedCorner)
        }
        
        if side.contains(.top) {
            self.addTopBorder(color: color, thickness: thickness, insets: insets, roundedCorner: roundedCorner)
        }
        
        if side.contains(.right) {
            self.addRightBorder(color: color, thickness: thickness, insets: insets, roundedCorner: roundedCorner)
        }
        
        if side.contains(.bottom) {
            self.addBottomBorder(color: color, thickness: thickness, insets: insets, roundedCorner: roundedCorner)
        }
    }
    
    private func addLeftBorder(color: UIColor, thickness: CGFloat, insets: Insets, roundedCorner: Bool) {
        
        let border = Border(color: color, thikness: thickness, insets: insets, roundedCorner: roundedCorner)
        border.translatesAutoresizingMaskIntoConstraints = false
        border.tag = Side.left.rawValue
        self.addSubview(border)
        
        var format = String(format: "V:|-%f-[view]-%f-|", insets.left, insets.right)
        
        let alignmentConstraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": border])
        
        format = String(format: "H:|-0-[view(%f)]", thickness)
        
        let widthConstraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": border])
        
        self.addConstraints(alignmentConstraints)
        self.addConstraints(widthConstraints)
    }
    
    private func addRightBorder(color: UIColor, thickness: CGFloat, insets: Insets, roundedCorner: Bool) {
        
        let border = Border(color: color, thikness: thickness, insets: insets, roundedCorner: roundedCorner)
        border.translatesAutoresizingMaskIntoConstraints = false
        border.tag = Side.right.rawValue
        self.addSubview(border)
        
        var format = String(format: "V:|-%f-[view]-%f-|", insets.left, insets.right)
        
        let alignmentConstraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": border])
        
        format = String(format: "H:[view(%f)]-0-|", thickness)
        
        let widthConstraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": border])
        
        self.addConstraints(alignmentConstraints)
        self.addConstraints(widthConstraints)
    }
    
    private func addTopBorder(color: UIColor, thickness: CGFloat, insets: Insets, roundedCorner: Bool) {
        
        let border = Border(color: color, thikness: thickness, insets: insets, roundedCorner: roundedCorner)
        border.translatesAutoresizingMaskIntoConstraints = false
        border.tag = Side.top.rawValue
        self.addSubview(border)
        
        var format = String(format: "H:|-%f-[view]-%f-|", insets.left, insets.right)
        
        let alignmentConstraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": border])
        
        format = String(format: "V:|-0-[view(%f)]", thickness)
        
        let widthConstraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": border])
        
        self.addConstraints(alignmentConstraints)
        self.addConstraints(widthConstraints)
    }
    
    private func addBottomBorder(color: UIColor, thickness: CGFloat, insets: Insets, roundedCorner: Bool) {
        
        let border = Border(color: color, thikness: thickness, insets: insets, roundedCorner: roundedCorner)
        border.translatesAutoresizingMaskIntoConstraints = false
        border.tag = Side.bottom.rawValue
        self.addSubview(border)
        
        var format = String(format: "H:|-%f-[view]-%f-|", insets.left, insets.right)
        
        let alignmentConstraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": border])
        
        format = String(format: "V:[view(%f)]-0-|", thickness)
        
        let widthConstraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": border])
        
        self.addConstraints(alignmentConstraints)
        self.addConstraints(widthConstraints)
    }
    
    func changeBorderColor(_ side: Side = .all, color: UIColor) {
        
        let borders = self.subviews.filter { $0 is Border } as! [Border]
        
        for border in borders {
            
            if side.contains(Side(rawValue: border.tag)) {
                border.color = color
            }
        }
    }
    
    func removeBorder(_ side: Side = .all) {
        
        let borders = self.subviews.filter { $0 is Border } as! [Border]
        
        for border in borders {
            
            if side.contains(Side(rawValue: border.tag)) {
                border.removeFromSuperview()
            }
        }
    }
}


public extension CALayer {
    
    class Border: CALayer, BorderProtocol {
        
        public var color: UIColor {
            
            didSet {
                self.borderColor = color.cgColor
            }
        }
        
        public var thickness: CGFloat {
            
            didSet {
                self.borderWidth = thickness
            }
        }
        
        public var insets: Insets
        public var tag: Int = 0
        
        fileprivate init(_ color: UIColor = .black, thickness: CGFloat = 1.0, insets: Insets = (0.0, 0.0)) {
            
            self.color = color
            self.thickness = thickness
            self.insets = insets
            
            super.init()
            
            self.needsDisplayOnBoundsChange = true
            self.borderColor = color.cgColor
            self.borderWidth = thickness
        }
        
        public override init(layer: Any) {
            
            self.color = .black
            self.thickness = 1.0
            self.insets = (0.0, 0.0)
            
            super.init(layer: layer)
        }
        
        required public init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    func addBorder(_ side: Side = .all, color: UIColor = .black, thickness: CGFloat = 1.0, insets: Insets = (0.0, 0.0)) {
        
        if side.contains(.left) {
            self.addLeftBorder(color: color, thickness: thickness, insets: insets)
        }
        
        if side.contains(.top) {
            self.addTopBorder(color: color, thickness: thickness, insets: insets)
        }
        
        if side.contains(.right) {
            self.addRightBorder(color: color, thickness: thickness, insets: insets)
        }
        
        if side.contains(.bottom) {
            self.addBottomBorder(color: color, thickness: thickness, insets: insets)
        }
    }
    
    private func addLeftBorder(color: UIColor, thickness: CGFloat, insets: Insets) {
        
        let border = Border.init(color, thickness: thickness, insets: insets)
        border.tag = Side.left.rawValue
        border.frame = CGRect(x: 0, y: insets.left, width: thickness, height: self.frame.height - (insets.left + insets.right))
        self.addSublayer(border)
    }
    
    private func addTopBorder(color: UIColor, thickness: CGFloat, insets: Insets) {
        
        let border = Border.init(color, thickness: thickness, insets: insets)
        border.tag = Side.top.rawValue
        border.frame = CGRect(x: insets.left, y: 0, width: self.frame.width - (insets.left + insets.right), height: thickness)
        self.addSublayer(border)
    }
    
    private func addRightBorder(color: UIColor, thickness: CGFloat, insets: Insets) {
        
        let border = Border.init(color, thickness: thickness, insets: insets)
        border.tag = Side.right.rawValue
        border.frame = CGRect(x:(self.frame.width - thickness), y: insets.left, width: thickness, height: self.frame.height - (insets.left + insets.right))
        self.addSublayer(border)
    }
    
    private func addBottomBorder(color: UIColor, thickness: CGFloat, insets: Insets) {
        
        let border = Border.init(color, thickness: thickness, insets: insets)
        border.tag = Side.bottom.rawValue
        border.frame = CGRect(x: insets.left, y: (self.frame.height - thickness), width: self.frame.width - (insets.left + insets.right), height: thickness)
        self.addSublayer(border)
    }
    
    func changeBorderColor(_ side: Side = .all, color: UIColor) {
        
        let borders = self.sublayers?.filter { $0 is Border } as! [Border]
        
        for border in borders {
            
            if side.contains(Side(rawValue: border.tag)) {
                border.color = color
            }
        }
    }
    
    func removeBorder(_ side: Side = .all) {
        
        let borders = self.sublayers?.filter { $0 is Border } as! [Border]
        
        for border in borders {
            
            if side.contains(Side(rawValue: border.tag)) {
                border.removeFromSuperlayer()
            }
        }
    }
}


