//
//  UIButton+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 21/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import UIKit

public extension UIButton {
    
    func setTitle(_ title: String?) {
        
        self.setTitle(title, for: .normal)
        self.setTitle(title, for: .highlighted)
        self.setTitle(title, for: .selected)
    }
    
    func setAttributedTitle(_ title: NSAttributedString?) {
        
        self.setAttributedTitle(title, for: .normal)
        self.setAttributedTitle(title, for: .highlighted)
        self.setAttributedTitle(title, for: .selected)
    }
    
    func setTitleColor(_ color: UIColor?) {
        
        self.setTitleColor(color, for: .normal)
        self.setTitleColor(color, for: .highlighted)
        self.setTitleColor(color, for: .selected)
    }
    
    func setImage(_ image: UIImage?) {
        
        self.setImage(image, for: .normal)
        self.setImage(image, for: .highlighted)
        self.setImage(image, for: .selected)
    }
    
    func setBackgroundImage(_ image: UIImage?) {
        
        self.setBackgroundImage(image, for: .normal)
        self.setBackgroundImage(image, for: .highlighted)
        self.setBackgroundImage(image, for: .selected)
    }
}

