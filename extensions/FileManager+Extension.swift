//
//  FileManager+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 14/03/18.
//  Copyright © 2018 scio. All rights reserved.
//

import Foundation


// MARK: - Paths for projects
public extension FileManager {
    
    var libraryURL: URL {
        
        let array = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)
        return URL.init(fileURLWithPath: array.first!)
    }
    
    var documentURL: URL {
        
        let array = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        return URL.init(fileURLWithPath: array.first!)
    }
    
    var cacheURL: URL {
        
        let array = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        return URL.init(fileURLWithPath: array.first!)
    }
}

public extension FileManager {
    
    func createFolder(_ path: URL) throws -> Bool {
        
        guard !self.fileExists(atPath: path.path) else { return true }
        
        do {
            
            try self.createDirectory(at: path, withIntermediateDirectories: true, attributes: nil)
            
            return true
            
        } catch {
            throw error
        }
    }
    
    func moveItem(from: URL, to: URL) throws -> Bool {
        
        guard self.fileExists(atPath: from.path) else { return false }
        
        if self.fileExists(atPath: to.path) {
            
            do {
                try self.removeItem(at: to)
            } catch {
                
            }
        }
        
        do {
            
            try self.moveItem(at: from, to: to)
            return true
            
        } catch {
            throw error
        }
    }
}
