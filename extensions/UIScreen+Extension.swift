//
//  UIScreen+Extension.swift
//  extensions
//
//  Created by Robo-Vikas on 21/02/18.
//  Copyright © 2018 scio. All rights reserved.
//

import UIKit

public extension UIScreen {
    
    var width: CGFloat { return self.bounds.size.width }
    var height: CGFloat { return self.bounds.size.height }
}
